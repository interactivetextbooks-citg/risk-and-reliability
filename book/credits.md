(credits)=
# Credits and License

You can refer to this book as:

> Lanzafame, R. (2024) _Risk and Reliability for Engineers_, TU Delft Open. https://doi.org/10.59490/tb.89.

The introduction, structure of the book and formatting of contents is done by the Editor, Robert Lanzafame. Some chapters and pages have additional primary authors who are identified within the book either at the bottom of the first page in a chapter, or at the bottom of an individual page, as necessary. If an author is not listed on a particular page, it is Robert Lanzafame.

You can refer to individual chapters or pages within this book as:

> `<Primary Author>` (2024) `<Title of Chapter or Page>`. In Robert Lanzafame (Ed.), _Risk and Reliability for Engineers._ TU Delft Open. https://doi.org/10.59490/tb.89.

Although content will be added over time, chapter titles and URL's in this book are expected to remain relatively static. However, we make no guarantee, so if it is important for you to reference a specific location within the book, we recommend including the complete URL and date of access in your reference.

## How the book is made

This book is created using open source tools: it is a Jupyter Book that is written using Markdown, Jupyter notebooks and Python files to generate some figures. The files are stored on a [public GitLab repository of TU Delft](https://gitlab.tudelft.nl/interactivetextbooks-citg/risk-and-reliability/). The published version of this book is compiled from a special branch (`publish-tud-open-textbook`). View the repository README file or contact the author for additional information.

(editor)=
## About the Editor

Robert Lanzafame is a Senior Lecturer at Delft University of Technology in the Netherlands. He enjoys teaching and finding new ways to incorporate digital tools in engineering education, including making online interactive textbooks like this one together with colleagues. For more about Robert, visit his [TU Delft page](https://www.tudelft.nl/en/staff/r.c.lanzafame/).

### Acknowledgements

This book uses adapted excerpts from the lecture notes of a previous course, CIE4130 Probabilistic Design, which was last taught at Delft University of Technology in 2022. In particular, parts of the risk analysis and most of the risk evaluation chapters reuse modified material from Professor Bas Jonkman. This book, and the included exercises, would not be possible without the efforts of many probabilistic design teachers over the last decades (in alphabetical order): S.N. (Bas) Jonkman, Han Vrijling, Oswaldo Morales Napoles, Pieter van Gelder, Raphaël Steenbergen, Robert Lanzafame, Ton Vrouwenvelder.

Many people provide technical support to enable teachers at the faculty of Civil Engineering and Geosciences (CEG) to develop books for improving the learning experience of our students, especially via development of the MUDE Module (CEGM1000) and through the activities of our teaching assistants. For additional information on books at CEG, contact Robert Lanzafame or Tom van Woudenberg at `books-CEG` AT `tudelft.nl` or visit [interactivetextbooks.citg.tudelft.nl](https://interactivetextbooks.citg.tudelft.nl/). Financial support has been generously granted by the CEG faculty via various grants and discretionary funds.

Special thanks goes to Caspar Jungbacker, who set up the JupyterBook and GitHub repository to make this book and website possible; Benjamin Ramousse, who brought the bivariate "patterns" to life with Python; and Thirza Feenstra, who, along with Caspar and Robert, thought that this whole Jupyter Book thing was a "good idea."

---

This book is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a> (CC BY), except the following.

CC BY conditions are not applicable to: {numref}`risk-curve-baecher`, {numref}`risk-curve-baecher-2` and {numref}`risk_matrix`.

{numref}`risk-curve-baecher` and {numref}`risk-curve-baecher-2` are from {cite:t}`baecher2003`, based on {cite:t}`baecher1982` and described in {cite:t}`baecher2021`.

{numref}`risk_matrix` is from an online PDF document (located [here](https://home.army.mil/lee/application/files/7815/3809/1878/Tables.pdf)). The formal definition for the risk matrix can be found in Engineering Manual EM 385-1-1 (located [here](https://www.publications.usace.army.mil/portals/76/publications/engineermanuals/em_385-1-1.pdf)).